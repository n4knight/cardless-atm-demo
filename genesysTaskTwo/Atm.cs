﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace genesysTaskTwo
{
    class Atm : User, IBank
    {

        User user = new User();

        public const double FIVE_H = 500;
        public const double THOUSAND_ = 1000;

        public double AtmBalance { get; } = 50000;

        public virtual void withdraw(double amountToWithdraw)
        {

            if (amountToWithdraw > AtmBalance || amountToWithdraw > Balance)
            {
                Console.WriteLine("insufficient funds");
                return;
            }


            while (amountToWithdraw % FIVE_H != 0 || amountToWithdraw % THOUSAND_ != 0)
            {
                Console.Write($"{amountToWithdraw} should be multiple of {FIVE_H} or {THOUSAND_}. Try again ");
                amountToWithdraw = Convert.ToDouble(Console.ReadLine());

            }

            Console.Write("enter your pin to withdraw ");
            var userpin = Convert.ToInt32(Console.ReadLine());

            while (userpin != Password)
            {
                Console.WriteLine("Wrong pin. Input again ");
                userpin = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Withdraw successful");
            Balance -= amountToWithdraw;
            checkBalance();

        }

        public virtual void addMoney (double amountToAdd)
        {
            Console.Write("enter your pin to add money ");
            var userpin = Convert.ToInt32(Console.ReadLine());

            while (userpin != Password)
            {
                Console.WriteLine("Wrong pin. Input again ");
                userpin = Convert.ToInt32(Console.ReadLine());
            }

            Balance += amountToAdd;
            Console.WriteLine($"your new balance: {Balance}");
        }

        public virtual void checkBalance()
        {
            Console.WriteLine($"Your balance: {Balance}");

        }

    }

    
}
