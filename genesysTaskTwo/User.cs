﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace genesysTaskTwo
{
     class User
    {
        private  int _password;
        private double _balance;
        private const int THOUSAND_ = 20000;

        public User()
        {
            _balance = THOUSAND_;
        }


        public int Password { get => _password; set => _password = value; }

        public double Balance { get => _balance; set => _balance = value; }

        public virtual void passwordSet()
        {
            Console.WriteLine("To begin, set your ATM pin");
            string pin = Console.ReadLine();

            while (pin.Length < 4 || pin.Length > 4)
            {
                Console.WriteLine("Pin length should be 4. set it again");
                pin = Console.ReadLine();
            }
            _password = Convert.ToInt32(pin);

        }

    }
}
