﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace genesysTaskTwo
{
     class Language : Atm

    {

        Atm atm = new Atm();

        public Language ()
        {
            atm.passwordSet();
        }
        void englishMenu()
        {
            Console.WriteLine("Welcome to soso&so Atm");
            Console.WriteLine("1. Withdraw Money\n2. Check Balance\n3. Add money");
        }

        void pidginMenu()
        {
            Console.WriteLine("Welcome to soso&so Atm");
            Console.WriteLine("1. Collect Money\n2. See account balance\n3. put money");
        }

        public void englishLanguage()
        {

        mainStart:
            englishMenu();

            try
            {
                var input = Convert.ToInt32(Console.ReadLine());


                while (input > 3)
                {
                    englishMenu();
                    input = Convert.ToInt32(Console.ReadLine());
                }

                switch (input)
                {
                    case 1:
                        Console.Write("Enter amount to withdraw ");
                        var amount2Withdraw = Convert.ToInt32(Console.ReadLine());
                        atm.withdraw(amount2Withdraw);
                        break;
                    case 2:
                        atm.checkBalance();
                        break;
                    case 3:
                        Console.Write("Enter amount to add ");
                        var amoun2Add = Convert.ToDouble(Console.ReadLine());
                        atm.addMoney(amoun2Add);
                        break;
                    default:
                        return;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message + " Try again");
                goto mainStart;
            }




        }

        public void pidginLanguage()
        {
        mainStart:
            pidginMenu();

            try
            {
                var input = Convert.ToInt32(Console.ReadLine());

                while (input > 3)
                {
                    pidginMenu();
                    input = Convert.ToInt32(Console.ReadLine());

                }

                switch (input)
                {
                    case 1:
                        Console.Write("Enter the amount you wan collect ");
                        var amount2Withdraw = Convert.ToDouble(Console.ReadLine());
                        withdraw(amount2Withdraw);
                        break;
                    case 2:
                        checkBalance();
                        break;
                    case 3:
                        Console.Write("Enter amount you wan put for account ");
                        var amoun2Add = Convert.ToDouble(Console.ReadLine());
                        addMoney(amoun2Add);
                        break;
                    default:
                        return;
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message + "Try again ");
                goto mainStart;
            }


        }

        public override void withdraw(double amountToWithdraw)
        {
            if (amountToWithdraw > atm.AtmBalance || amountToWithdraw > atm.Balance)
            {
                Console.WriteLine("Money no too plenty for account");
                return;
            }

            while (amountToWithdraw % FIVE_H != 0 || amountToWithdraw % THOUSAND_ != 0)
            {
                Console.Write($"{amountToWithdraw} no be multiple of {FIVE_H} or {THOUSAND_}. Try again ");
                amountToWithdraw = Convert.ToDouble(Console.ReadLine());

            }


            Console.Write("enter your pin to collect your money ");
            var userpin = Convert.ToInt32(Console.ReadLine());

            while (userpin != atm.Password)
            {
                Console.WriteLine("You put wrong pin. do am again ");
                userpin = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Collect your money");
            atm.Balance -= amountToWithdraw;
            checkBalance();

        }

        public override void addMoney(double amountToAdd)
        {
            Console.Write("enter your pin to put money for account ");
            var userpin = Convert.ToInt32(Console.ReadLine());

            while (userpin != atm.Password)
            {
                Console.WriteLine("You put wrong pin. do am again ");
                userpin = Convert.ToInt32(Console.ReadLine());
            }

            atm.Balance += amountToAdd;
            Console.WriteLine($"Your new balance: {atm.Balance}");
        }

        public override void checkBalance()
        {
            Console.WriteLine($"Your balance: {atm.Balance}");

        }

        public static bool isNumber(string input)
        {
            return int.TryParse(input, out int result);
        }


    }


}
