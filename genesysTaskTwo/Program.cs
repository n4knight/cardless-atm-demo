﻿using System;
using System.Text.RegularExpressions;

namespace genesysTaskTwo
{
    class Program
    {


        static void Main(string[] args)
        {
            Language atmmachine = new Language();

            atmMainMenu();
            try
            {
                var input = Convert.ToInt32(Console.ReadLine());
                while (input > 2)
                {
                    Console.Write("invalid option. Choose again ");
                    atmMainMenu();
                    input = Convert.ToInt32(Console.ReadLine());
                }

                switch (input)
                {
                    case 1:
                        atmmachine.englishLanguage();
                        break;
                    case 2:
                        atmmachine.pidginLanguage();
                        break;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);

                Console.Write("Use numbers this time ");
                var input = Convert.ToInt32(Console.ReadLine());
                while (input > 2)
                {
                    Console.Write("invalid option. Choose again ");
                    atmMainMenu();
                    input = Convert.ToInt32(Console.ReadLine());
                }

                switch (input)
                {
                    case 1:
                        atmmachine.englishLanguage();
                        break;
                    case 2:
                        atmmachine.pidginLanguage();
                        break;
                }
            }
            




        }

        public static void atmMainMenu ()
        {
            Console.WriteLine("Choose your language.\n1. English \n2. pidgin");
        }


    }



}
